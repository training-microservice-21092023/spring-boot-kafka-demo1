package id.co.pkp.springbootkafkademo1.model;

import id.co.pkp.springbootkafkademo1.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo1
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
@Repository
@RequiredArgsConstructor
public class EventStore {

    private final EventRepository repository;

    public void save(ProcessedEvent event) {
        this.repository.save(event);
    }

    public List<String> findAll() {
        return repository.findAll().stream()
                .map(ProcessedEvent::getId)
                .toList();
    }
}
