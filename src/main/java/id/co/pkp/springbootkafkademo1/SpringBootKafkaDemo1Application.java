package id.co.pkp.springbootkafkademo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootKafkaDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootKafkaDemo1Application.class, args);
    }

}
