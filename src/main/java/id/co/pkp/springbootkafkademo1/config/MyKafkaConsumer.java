package id.co.pkp.springbootkafkademo1.config;

import id.co.pkp.springbootkafkademo1.model.EventStore;
import id.co.pkp.springbootkafkademo1.model.ProcessedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo1
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 12:13
 * To change this template use File | Settings | File Templates.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class MyKafkaConsumer {

    private final String groupId = "test-group-basic-consumer";
    private final EventStore eventStore;

    @KafkaListener(topics = {"pkp-dev"}, containerFactory = "basicListenerContainerFactory", groupId = groupId)
    public void listen(String message) {
        log.info("[" + groupId + "] basic consumer : " + message);
        eventStore.save(new ProcessedEvent(message, message));
        // handle business
    }
}
