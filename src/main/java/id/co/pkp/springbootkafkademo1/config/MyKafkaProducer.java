package id.co.pkp.springbootkafkademo1.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo1
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
@Component
@RequiredArgsConstructor
public class MyKafkaProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;
    @Value("${kafka.topic:sample-topic}")
    private String topic;

    public void sendMessage(String message) {
        kafkaTemplate.send(topic, message);
    }
}
