package id.co.pkp.springbootkafkademo1.controller;

import id.co.pkp.springbootkafkademo1.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 08:18
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("kafka")
@RequiredArgsConstructor
@Transactional
public class UserController {
    private static final String TOPIC = "hendi-dev";
    private final KafkaTemplate<String, User> kafkaTemplate;

    @GetMapping("publish/{name}")
    public String post(@PathVariable("name") final String name) {
        kafkaTemplate.send(TOPIC, new User(name, "Technology", 12000L));
        return "Published successfully";
    }

}
