package id.co.pkp.springbootkafkademo1.controller;

import com.google.gson.Gson;
import id.co.pkp.springbootkafkademo1.model.Book;
import id.co.pkp.springbootkafkademo1.model.MoreSimpleModel;
import id.co.pkp.springbootkafkademo1.model.SimpleModel;
import id.co.pkp.springbootkafkademo1.service.KafkaConsumer;
import id.co.pkp.springbootkafkademo1.service.KafkaProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 06:24
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/api/kafka")
@RequiredArgsConstructor
@Slf4j
public class KafkaController {
    private final KafkaTemplate<String, String> kafkaTemplate;

    private final KafkaProducer producer;

    private final Gson jsonConverter;

    @PostMapping("/send")
    public void send(@RequestBody String data) {
        producer.produce(data);
    }

    @GetMapping("/receive")
    public List<String> receive() {
        return KafkaConsumer.messages;
    }

    @PostMapping(value = "/publish")
    public ResponseEntity<String> publishMessage(@RequestBody String message,
                                                 @RequestHeader(name = "topic-name") String topicName) {
        producer.publishMessage(message, topicName);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/books/publish")
    public ResponseEntity<String> publishMessage(@RequestBody Book book,
                                                 @RequestHeader(name = "topic-name") String topicName) {
        producer.publishBooksUpdateMessage(book, topicName);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public void post(@RequestBody SimpleModel simpleModel) {
        kafkaTemplate.send("myTopic", jsonConverter.toJson(simpleModel));
    }

    @PostMapping("/v2")
    public void postV2(@RequestBody MoreSimpleModel moreSimpleModel) {
        kafkaTemplate.send("myTopic2", jsonConverter.toJson(moreSimpleModel));
    }

    @KafkaListener(topics = "myTopic")
    public void getFromKafka(String simpleModel) {
        log.info(simpleModel);
        SimpleModel simpleModel1 = jsonConverter.fromJson(simpleModel, SimpleModel.class);
        log.info(simpleModel1.toString());
    }

    @KafkaListener(topics = "myTopic2")
    public void getFromKafka2(String moreSimpleModel) {
        log.info(moreSimpleModel);
        MoreSimpleModel simpleModel1 = jsonConverter.fromJson(moreSimpleModel, MoreSimpleModel.class);
        log.info(simpleModel1.toString());
    }
}
