package id.co.pkp.springbootkafkademo1.controller;

import id.co.pkp.springbootkafkademo1.config.MyKafkaProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo1
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("api/kafka")
@RequiredArgsConstructor
@Slf4j
public class ProducerController {

    private final MyKafkaProducer producer;

    @GetMapping("/send")
    public String sendUsingSimpleProducer() {
        producer.sendMessage(UUID.randomUUID().toString());
        log.info("Producer Send: " + LocalDateTime.now());
        return "Producer Send: " + LocalDateTime.now();
    }
}
