package id.co.pkp.springbootkafkademo1.controller;

import id.co.pkp.springbootkafkademo1.model.EventStore;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo1
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/consumer")
@RequiredArgsConstructor
public class ConsumerController {

    private final EventStore eventStore;

    @GetMapping("/all")
    public List<String> all() {
        return eventStore.findAll();
    }
}
