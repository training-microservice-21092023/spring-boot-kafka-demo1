package id.co.pkp.springbootkafkademo1.repository;

import id.co.pkp.springbootkafkademo1.model.ProcessedEvent;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-demo1
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 12:11
 * To change this template use File | Settings | File Templates.
 */
public interface EventRepository extends JpaRepository<ProcessedEvent, String> {
}
